package db

import (
	"database/sql"
	"log"
	"time"
)

type Book struct {
	Id          int64     `db:"id" json:"id"`
	Name        string    `db:"name" json:"name"`
	Author      string    `db:"author" json:"author"`
	ImageUrl    string    `db:"image_url" json:"image_url"`
	Description string    `db:"description" json:"description"`
	BookType    int64     `db:"book_type" json:"book_type"`
	UpdateTime  time.Time `db:"update_time" json:"update_time"`
}

type BookAndUrl struct {
	Id        int64  `db:"id" json:"id"`
	Name      string `db:"name" json:"name"`
	BookUrlId int64  `db:"book_url_id" json:"book_url_id"`
	FromUrl   string `db:"from_url" json:"from_url"`
	LinkUrl   string `db:"link_url" json:"link_url"`
	LinkText  string `db:"link_text" json:"link_text"`
}

type BookCreateReq struct {
	Id          int64  `db:"id" json:"id"`
	Name        string `db:"name" json:"name"`
	Author      string `db:"author" json:"author"`
	ImageUrl    string `db:"image_url" json:"image_url"`
	Description string `db:"description" json:"description"`
	BookType    string `db:"book_type" json:"book_type"`
}

type BookAndType struct {
	Id          int64     `db:"id" json:"id"`
	Name        string    `db:"name" json:"name"`
	Author      string    `db:"author" json:"author"`
	ImageUrl    string    `db:"image_url" json:"image_url"`
	Description string    `db:"description" json:"description"`
	UpdateTime  time.Time `db:"update_time" json:"update_time"`
	BookTypeId  int64     `db:"book_type_id" json:"book_type_id"`
	BookType    string    `db:"book_type" json:"book_type"`
}

type BookArticle struct {
	Id        int64  `db:"id" json:"id"`
	Name      string `db:"name" json:"name"`
	Author    string `db:"author" json:"author"`
	ArticleId int64  `db:"article_id" json:"article_id"`
	Title     string `db:"title" json:"title"`
	Text      string `db:"text" json:"text"`
}

type BookDetail struct {
	Id          int64            `db:"id" json:"id"`
	Name        string           `db:"name" json:"name"`
	Author      string           `db:"author" json:"author"`
	ImageUrl    string           `db:"image_url" json:"image_url"`
	Description string           `db:"description" json:"description"`
	UpdateTime  time.Time        `db:"update_time" json:"update_time"`
	BookTypeId  int64            `db:"book_type_id" json:"book_type_id"`
	BookType    string           `db:"book_type" json:"book_type"`
	NewSection  []ArticleSection `db:"new_section" json:"new_section"`
}

func CreateBook(data *BookCreateReq) (int64, error) {

	bookType, err := FindBookType(data.BookType)
	if err != nil {
		return 0, err
	}

	book := new(Book)
	book.Name = data.Name
	book.Author = data.Author
	book.ImageUrl = data.ImageUrl
	book.Description = data.Description

	if bookType != nil {
		book.BookType = bookType.Id

	} else {
		bookTypeId, err := CreateBookType(data.BookType)
		if err != nil {
			return 0, err
		}
		book.BookType = bookTypeId
	}
	return CreateBookDO(book)
}

func CreateBookDO(data *Book) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`INSERT INTO book(
name,
author,
image_url,
description,
book_type
) values(?,?,?,?,?)`,
		data.Name,
		data.Author,
		data.ImageUrl,
		data.Description,
		data.BookType)

	if err != nil {
		log.Printf("CreateBook Inset failed, err: %v\n", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("CreateBook Get lastInsertID failed, err: %v\n", err)
		return 0, err
	}

	_, err = result.RowsAffected()
	if err != nil {
		log.Printf("CreateBook get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return lastInsertID, nil
}

func UpdateBookStatus(id int64, status string) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`update book set status =? where id=?`, status, id)
	if err != nil {
		log.Printf("updatebook failed, err:%v\n", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("updatebook get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return rowsAffected, nil
}

func FindAllBook() ([]BookAndUrl, error) {
	db := GetDB()

	rows, err := db.Query(`select b.id,b.name,u.id,u.link_url,u.link_text,u.from_url from book b left JOIN  book_url u on b.id = u.book_id where u.status=""`)

	if err != nil {
		log.Printf("FindAllBook query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []BookAndUrl

	for rows.Next() {
		app := new(BookAndUrl)
		err = rows.Scan(&app.Id, &app.Name, &app.BookUrlId, &app.LinkUrl, &app.LinkText, &app.FromUrl)

		if err != nil {
			log.Printf("FindAllBook rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	return results, nil
}

func FindBookList(typeId int64) ([]BookAndType, error) {
	db := GetDB()

	var rows *sql.Rows
	var err error

	if typeId != 0 {
		rows, err = db.Query(`select b.id,b.name,b.author,b.image_url,b.description,b.update_time,b.book_type,t.name from book b join book_type t on b.book_type = t.id
where b.status="success" and b.book_type=? order by b.update_time desc`, typeId)
	} else {
		rows, err = db.Query(`select b.id,b.name,b.author,b.image_url,b.description,b.update_time,b.book_type,t.name from book b join book_type t on b.book_type = t.id
where b.status="success" order by b.update_time desc`)
	}

	if err != nil {
		log.Printf("FindAllBook query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []BookAndType

	for rows.Next() {
		app := new(BookAndType)
		err = rows.Scan(&app.Id, &app.Name, &app.Author, &app.ImageUrl, &app.Description, &app.UpdateTime, &app.BookTypeId, &app.BookType)

		if err != nil {
			log.Printf("FindBookList rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	return results, nil
}

func FindBookById(bookId int64) (*Book, error) {
	db := GetDB()

	app := new(Book)

	row := db.QueryRow("select id,name,author,image_url,description,update_time from book where id =? ", bookId)

	if err := row.Scan(&app.Id, &app.Name, &app.Author, &app.ImageUrl, &app.Description, &app.UpdateTime); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindBookById scan failed, err:%v\n", err)
		return nil, err
	}

	return app, nil
}

func FindBookByNameAndAuthor(name string, author string) (*Book, error) {
	db := GetDB()

	app := new(Book)

	row := db.QueryRow("select id from book where name =? and author =? ", name, author)

	if err := row.Scan(&app.Id); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindBookByName scan failed, err:%v\n", err)
		return nil, err
	}

	return app, nil
}

func GetBookDetail(id int64) (*BookDetail, error) {
	db := GetDB()

	app := new(BookDetail)

	row := db.QueryRow(`select b.id,b.name,b.author,b.image_url,b.description,b.update_time,b.book_type,t.name from book b join book_type t on b.book_type = t.id
where b.status="success" and b.id=? order by b.update_time desc`, id)

	if err := row.Scan(&app.Id, &app.Name, &app.Author, &app.ImageUrl, &app.Description, &app.UpdateTime, &app.BookTypeId, &app.BookType); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("GetBookDetail scan failed, err:%v\n", err)
		return nil, err
	}

	rows, err := db.Query(`select
id,
title,
book_id ,
book_url_id
from article where book_id=? order by book_url_id DESC limit 5 `, id)
	if err != nil {
		log.Printf("GetBookDetail query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []ArticleSection

	for rows.Next() {
		app := new(ArticleSection)
		err = rows.Scan(&app.Id,
			&app.Title,
			&app.BookId,
			&app.BookUrlId,
		)

		if err != nil {
			log.Printf("GetBookDetail rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}
	app.NewSection = results
	return app, nil
}
