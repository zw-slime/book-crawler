package db

import (
	"database/sql"
	"log"
)

type BookType struct {
	Id   int64  `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

func CreateBookType(data string) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`INSERT INTO book_type(
name
) values(?)`,
		data)

	if err != nil {
		log.Printf("CreateBookType Inset failed, err: %v\n", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("CreateBookType Get lastInsertID failed, err: %v\n", err)
		return 0, err
	}

	_, err = result.RowsAffected()
	if err != nil {
		log.Printf("CreateBookType get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return lastInsertID, nil
}

func FindBookType(name string) (*BookType, error) {
	db := GetDB()
	bookType := new(BookType)

	row := db.QueryRow("select id,name from book_type where name=? ", name)

	if err := row.Scan(&bookType.Id, &bookType.Name); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindBookType scan failed, err:%v\n", err)
		return nil, err
	}
	return bookType, nil
}

func FindAllType() ([]BookType, error) {
	db := GetDB()

	rows, err := db.Query(`select id,name from book_type b`)

	if err != nil {
		log.Printf("FindAllType query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []BookType

	for rows.Next() {
		app := new(BookType)
		err = rows.Scan(&app.Id, &app.Name)

		if err != nil {
			log.Printf("FindAllType rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	return results, nil
}
