package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
	"sync"
)

var db *sql.DB
var once sync.Once

func GetDB() *sql.DB {
	once.Do(Init)
	return db
}

var dbDsn = os.Getenv("DB_DSN")

func Init() {

	var err error
	db, err = sql.Open("mysql", dbDsn+"?charset=utf8&parseTime=true")
	if err != nil {
		log.Fatalf("数据库连接失败：%v\n", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("数据库连接失败：%v\n", err)
	}
	log.Println("初始化数据库成功")
}
