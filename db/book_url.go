package db

import (
	"database/sql"
	"log"
)

type BookUrl struct {
	Id       int64  `db:"id" json:"id"`
	FromUrl  string `db:"from_url" json:"from_url"`
	LinkUrl  string `db:"link_url" json:"link_url"`
	LinkText string `db:"link_text" json:"link_text"`
	BookId   int64  `db:"book_id" json:"book_id"`
}

func CreateBookUrl(data *BookUrl) (int64, error) {

	db := GetDB()

	result, err := db.Exec(`INSERT INTO book_url(
from_url,
link_url,
link_text,
book_id
) values(?,?,?,?)`,
		data.FromUrl,
		data.LinkUrl,
		data.LinkText,
		data.BookId)

	if err != nil {
		log.Printf("CreateBookUrl Inset failed, err: %v\n", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("CreateBookUrl Get lastInsertID failed, err: %v\n", err)
		return 0, err
	}

	_, err = result.RowsAffected()
	if err != nil {
		log.Printf("CreateBookUrl get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return lastInsertID, nil
}

func FindBook(link_url, from_url string) (*BookUrl, error) {
	db := GetDB()
	app := new(BookUrl)

	row := db.QueryRow("select id from book_url where link_url=? and from_url=? ", link_url, from_url)

	if err := row.Scan(&app.Id); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindBook scan failed, err:%v\n", err)
		return nil, err
	}
	return app, nil
}

func FindBookUrl(bookId int) (*BookUrl, error) {
	db := GetDB()
	app := new(BookUrl)

	row := db.QueryRow("select id,link_url,from_url from book_url where book_id=? ", bookId)

	if err := row.Scan(&app.Id, &app.LinkUrl, &app.FromUrl); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindBookUrl scan failed, err:%v\n", err)
		return nil, err
	}
	return app, nil
}

//func DeleteApp(id int64) (int64, error) {
//	db := GetDB()
//
//	result, err := db.Exec(`DELETE FROM application WHERE id=?`, id)
//	if err != nil {
//		log.Printf("%d, application delete  failed, err: %v\n", id, err)
//		return 0, err
//	}
//
//	rowsAffected, err := result.RowsAffected()
//	if err != nil {
//		log.Printf("%d, application delete get RowsAffected failed, err: %v\n", id, err)
//		return 0, err
//	}
//
//	return rowsAffected, nil
//}
//
//func FindApp(appId int) (*App, error) {
//	db := GetDB()
//	app := new(App)
//
//	row := db.QueryRow("select id,app_id from application where app_id=? ", appId)
//
//	if err := row.Scan(&app.Id, &app.AppId); err != nil {
//		if err == sql.ErrNoRows {
//			return nil, nil
//		}
//		log.Printf("scan failed, err:%v\n", err)
//		return nil, err
//	}
//	return app, nil
//}
//
//func FindAllAppByPage(applicationType string, pn int, ps int, query string) (*Result, error) {
//	db := GetDB()
//
//	sql := `select
//id,
//record_time,
//icon,
//app_name,
//first_listed_time,
//update_time,
//app_type,
//developer,
//description,
//is_link_app,
//distribution_area,
//type,
//show_screenshot,
//sample_hash
//from application where`
//
//	if applicationType == "featured" {
//		sql = sql + " type='" + applicationType + "' and"
//	}
//
//	rows, err := db.Query(sql+" app_name  LIKE ? order by record_time DESC limit ? offset ?", "%"+query+"%", ps, ps*(pn-1))
//
//	if err != nil {
//		logex.Errorf("findAllAppByPage query err: %v\n", err)
//		return nil, err
//	}
//	defer rows.Close()
//	var results []AppList
//
//	for rows.Next() {
//		app := new(AppList)
//		err = rows.Scan(&app.Id,
//			&app.RecordTime,
//			&app.Icon,
//			&app.AppName,
//			&app.FirstListedTime,
//			&app.UpdateTime,
//			&app.AppType,
//			&app.Developer,
//			&app.Description,
//			&app.IsLinkApp,
//			&app.DistributionArea,
//			&app.Type,
//			&app.ShowScreenshot,
//			&app.SampleHash,
//		)
//
//		if err != nil {
//			logex.Errorf("findAllAppByPage rows.next err: %v\n", err)
//			return nil, err
//		}
//		if applicationType == "gray" {
//			app.Type = "gray"
//		}
//
//		results = append(results, *app)
//	}
//
//	sql1 := "select count(*) from application where "
//	if applicationType == "featured" {
//		sql1 += `type='` + applicationType + "' and"
//	}
//
//	var count int
//	row := db.QueryRow(sql1+" app_name  LIKE ?", "%"+query+"%")
//	err = row.Scan(&count)
//	if err != nil {
//		logex.Errorf("Query count * err: %v\n", err)
//		return nil, err
//	}
//
//	resultData := new(Result)
//	resultData.Pn = pn
//	resultData.Ps = ps
//	resultData.Total = count
//	resultData.Data = results
//
//	return resultData, nil
//}
//
//func FindAppDetailById(id int64, applicationType string) (*AppFront, error) {
//	db := GetDB()
//	app := new(AppFront)
//
//	row := db.QueryRow(`select id,
//record_time,
//icon,
//app_name,
//first_listed_time,
//update_time,
//app_type,
//developer,
//description,
//is_link_app,
//distribution_area,
//type,
//show_screenshot,
//sample_hash
//from application where id=?`, id)
//
//	if err := row.Scan(&app.Id, &app.RecordTime, &app.Icon, &app.AppName, &app.FirstListedTime,
//		&app.UpdateTime, &app.AppType, &app.Developer, &app.Description,
//		&app.IsLinkApp, &app.DistributionArea, &app.Type, &app.ShowScreenshot, &app.SampleHash); err != nil {
//		if err == sql.ErrNoRows {
//			return nil, nil
//		}
//		log.Printf("FindAppDetail failed, err:%v\n", err)
//		return nil, err
//	}
//
//	if app.Type == "gray" || applicationType == "gray" {
//		app.Type = "gray"
//		return app, nil
//	}
//
//	applicationId := []int64{id}
//	results2, err := FindScreenshotByApplicationId(applicationId)
//	if err != nil {
//		return nil, err
//	}
//
//	app.AnalysisScreenshot = results2
//	return app, nil
//}
//

func UpdateBookUrlStatus(id int64, status string) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`update book_url set status =? where id=?`, status, id)
	if err != nil {
		log.Printf("updatebook failed, err:%v\n", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("updatebook get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return rowsAffected, nil
}
