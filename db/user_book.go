package db

import (
	"fmt"
	"log"
)

type UserBook struct {
	Id     int64  `db:"id" json:"id"`
	BookId int64  `db:"book_id" json:"book_id"`
	Openid string `db:"openid" json:"openid"`
}

type UserBookList struct {
	Id          int64  `db:"id" json:"id"`
	BookId      int64  `db:"book_id" json:"book_id"`
	Name        string `db:"name" json:"name"`
	Author      string `db:"author" json:"author"`
	ImageUrl    string `db:"image_url" json:"image_url"`
	Description string `db:"description" json:"description"`
}

func CreateUserBook(data *UserBook) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`INSERT INTO user_book(
book_id,
openid
) values(?,?)`,
		data.BookId,
		data.Openid)

	if err != nil {
		log.Printf("CreateUserBook Inset failed, err: %v\n", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("CreateUserBook Get lastInsertID failed, err: %v\n", err)
		return 0, err
	}

	_, err = result.RowsAffected()
	if err != nil {
		log.Printf("CreateUserBook get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return lastInsertID, nil
}

func DeleteUserBook(id int64) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`delete from user_book where id=?`, id)

	if err != nil {
		log.Printf("DeleteUserBook Inset failed, err: %v\n", err)
		return 0, err
	}

	affect, err := result.RowsAffected()
	if err != nil {
		log.Printf("DeleteUserBook get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return affect, nil
}

func FindAllUserBook(openid string) ([]UserBookList, error) {
	db := GetDB()

	rows, err := db.Query(`select u.id,u.book_id,b.name,b.author,b.image_url,b.description,b.update_time from user_book u join book b on u.book_id = b.id
 where u.openid=?`, openid)

	if err != nil {
		log.Printf("FindAllUserBook query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []UserBookList
	fmt.Println(rows)
	for rows.Next() {
		app := new(UserBookList)
		err = rows.Scan(&app.Id, &app.BookId, &app.Name, &app.Name, &app.Author, &app.ImageUrl, &app.Description)

		if err != nil {
			log.Printf("FindAllUserBook rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	return results, nil
}
