package db

import (
	"database/sql"
	"log"
)

type Article struct {
	Id        int64  `db:"id" json:"id"`
	Title     string `db:"title" json:"title"`
	Text      string `db:"text" json:"text"`
	BookId    int64  `db:"book_id" json:"book_id"`
	BookUrlId int64  `db:"book_url_id" json:"book_url_id"`
}

type ArticleSection struct {
	Id        int64  `db:"id" json:"id"`
	Title     string `db:"title" json:"title"`
	BookId    int64  `db:"book_id" json:"book_id"`
	BookUrlId int64  `db:"book_url_id" json:"book_url_id"`
}

type CreateArticleReq struct {
	Title     string `db:"title" json:"title"`
	Text      string `db:"text" json:"text"`
	BookId    int64  `db:"book_id" json:"book_id"`
	BookUrlId int64  `db:"book_url_id" json:"book_url_id"`
}

type SectionResult struct {
	Data  []ArticleSection `json:"data"`
	Total int              `json:"total"`
}

type Result struct {
	Data  []Article `json:"data"`
	Ps    int       `json:"ps"`
	Pn    int       `json:"pn"`
	Total int       `json:"total"`
}

func CreateArticle(data *CreateArticleReq) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`INSERT INTO article(
title,
text,
book_id,
book_url_id
) values(?,?,?,?)`,
		data.Title,
		data.Text,
		data.BookId,
		data.BookUrlId)

	if err != nil {
		log.Printf("CreateArticle Inset failed, err: %v\n", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("CreateArticle Get lastInsertID failed, err: %v\n", err)
		return 0, err
	}

	_, err = result.RowsAffected()
	if err != nil {
		log.Printf("CreateArticle get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return lastInsertID, nil
}

// 获取目录
func FindArticleSection(bookId int64) (*SectionResult, error) {
	db := GetDB()

	sql := `select
id,
title,
book_id ,
book_url_id
from article where book_id=? order by book_url_id ASC `

	rows, err := db.Query(sql, bookId)

	if err != nil {
		log.Printf("FindArticleSection query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []ArticleSection

	for rows.Next() {
		app := new(ArticleSection)
		err = rows.Scan(&app.Id,
			&app.Title,
			&app.BookId,
			&app.BookUrlId,
		)

		if err != nil {
			log.Printf("FindArticleSection rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	sql1 := "select count(*) from article where book_id=?"

	var count int
	row := db.QueryRow(sql1, bookId)
	err = row.Scan(&count)
	if err != nil {
		log.Printf("Query count * err: %v\n", err)
		return nil, err
	}

	resultData := new(SectionResult)
	resultData.Total = count
	resultData.Data = results

	return resultData, nil
}

// 获取单片文章内容
func FindArticleById(id int64) (*Article, error) {
	db := GetDB()

	row := db.QueryRow(`select
id,
title,
text,
book_id ,
book_url_id
from article where id=?`, id)

	app := new(Article)

	if err := row.Scan(&app.Id, &app.Title, &app.Text, &app.BookUrlId, &app.BookId); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		log.Printf("FindArticleById scan failed, err:%v\n", err)
		return nil, err
	}

	return app, nil
}

func UpdateText(id int64, text string) (int64, error) {
	db := GetDB()

	result, err := db.Exec(`update article set text =? where id=?`, text, id)
	if err != nil {
		log.Printf("updateText failed, err:%v\n", err)
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		log.Printf("updateText get RowsAffected failed, err: %v\n", err)
		return 0, err
	}

	return rowsAffected, nil
}

func FindAllArticle() ([]Article, error) {
	db := GetDB()

	rows, err := db.Query(`select id,
title,
text,
book_id,
book_url_id
from article`)

	if err != nil {
		log.Printf("findAllArticle query err: %v\n", err)
		return nil, err
	}
	defer rows.Close()
	var results []Article

	for rows.Next() {
		app := new(Article)
		err = rows.Scan(&app.Id,
			&app.Title,
			&app.Text,
			&app.BookId,
			&app.BookUrlId,
		)

		if err != nil {
			log.Printf("findAllArticle rows.next err: %v\n", err)
			return nil, err
		}

		results = append(results, *app)
	}

	return results, nil
}
