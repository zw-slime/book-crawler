package controller

import (
	"book-crawler/db"
	"book-crawler/tools"
	"github.com/kataras/iris"
	"log"
)

type FindBookParam struct {
	TypeId int64 `json:"type_id"`
}

func FindBookListController(ctx iris.Context) {
	var result []db.BookAndType

	var param FindBookParam
	err := ctx.ReadJSON(&param)
	if err != nil {
		log.Printf("请求参数解析出错：%v\n", err)
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	result, err = db.FindBookList(param.TypeId)
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(result, nil, 0))
	if err != nil {
		return
	}
}
