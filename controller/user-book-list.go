package controller

import (
	"book-crawler/db"
	"book-crawler/tools"
	"errors"
	"github.com/kataras/iris"
	"log"
)

type UserBookListParam struct {
	Openid string `json:"openid"`
}

func UserBookListController(ctx iris.Context) {
	var param UserBookListParam
	err := ctx.ReadJSON(&param)
	if err != nil {
		log.Printf("请求参数解析出错：%v\n", err)
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	if param.Openid == "" {
		log.Println("openid不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("openid不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	data, err := db.FindAllUserBook(param.Openid)

	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(data, nil, 0))
	if err != nil {
		return
	}

}
