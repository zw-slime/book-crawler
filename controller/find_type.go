package controller

import (
	"book-crawler/db"
	"book-crawler/tools"
	"github.com/kataras/iris"
)

func FindBookTypeController(ctx iris.Context) {
	var result []db.BookType

	result, err := db.FindAllType()
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(result, nil, 0))
	if err != nil {
		return
	}
}
