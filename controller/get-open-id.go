package controller

import (
	"book-crawler/tools"
	"errors"
	"fmt"
	"github.com/kataras/iris"
	"io/ioutil"
	"log"
	"net/http"
)

type GetOpenIdParam struct {
	AppId  string `json:"appid"`
	Secret string `json:"secret"`
	Code   string `json:"code"`
}

func GetOpenId(ctx iris.Context) {

	var param GetOpenIdParam
	err := ctx.ReadJSON(&param)
	if err != nil {
		log.Printf("请求参数解析出错：%v\n", err)
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	if param.AppId == "" {
		log.Println("openid不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("appid不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}
	if param.Code == "" {
		log.Println("openid不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("code不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}
	if param.Secret == "" {
		log.Println("openid不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("secret不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	resp, err := http.Get("https://api.weixin.qq.com/sns/jscode2session?appid=" + param.AppId + "&secret=" + param.Secret + "&js_code=" + param.Code + "&grant_type=authorization_code")

	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}
	defer resp.Body.Close()

	fmt.Println(resp.Body)

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(result, nil, 0))
	if err != nil {
		return
	}
}
