package controller

import (
	"book-crawler/db"
	"book-crawler/tools"
	"github.com/kataras/iris"
)

type QueryParam struct {
	BookId int64 `json:"book_id"`
	Pn     int   `json:"pn"`
	Ps     int   `json:"ps"`
}

func ArticleSectionListController(ctx iris.Context) {
	var result *db.SectionResult

	id, err := ctx.Params().GetInt("id")

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		_, err := ctx.JSON(tools.WrapResult(nil, err, iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	result, err = db.FindArticleSection(int64(id))
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(result, nil, 0))
	if err != nil {
		return
	}
}
