package controller

import (
	"book-crawler/db"
	"book-crawler/tools"
	"errors"
	"github.com/kataras/iris"
	"log"
)

type CreateUserBookParam struct {
	OpenId string `json:"openid"`
	BookId int64  `json:"book_id"`
}

func CreateUserBookController(ctx iris.Context) {
	var param CreateUserBookParam
	err := ctx.ReadJSON(&param)
	if err != nil {
		log.Printf("请求参数解析出错：%v\n", err)
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	if param.OpenId == "" {
		log.Println("openid不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("openid不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	if param.BookId == 0 {
		log.Println("book_id不能为空")
		ctx.StatusCode(iris.StatusBadRequest)
		_, err = ctx.JSON(tools.WrapResult(nil, errors.New("book_id不能为空"), iris.StatusBadRequest))
		if err != nil {
			return
		}
		return
	}

	app := new(db.UserBook)
	app.Openid = param.OpenId
	app.BookId = param.BookId

	id, err := db.CreateUserBook(app)
	if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		_, err = ctx.JSON(tools.WrapResult(nil, err, iris.StatusInternalServerError))
		if err != nil {
			return
		}
		return
	}

	ctx.StatusCode(iris.StatusOK)
	_, err = ctx.JSON(tools.WrapResult(id, nil, 0))
	if err != nil {
		return
	}

}
