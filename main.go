package main

import (
	"book-crawler/controller"
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
)

func main() {
	app := iris.New()
	app.Get("/", func(ctx context.Context) {})

	app.Post("/book-list", controller.FindBookListController)

	app.Get("/book/{id}", controller.FindBookController)

	app.Get("/article-section/{id}", controller.ArticleSectionListController)

	app.Get("/article/{id}", controller.FindArticleController)

	app.Get("/book-type", controller.FindBookTypeController)

	app.Get("/book-detail/{id}", controller.BookDetailController)

	app.Post("/add-user-book", controller.CreateUserBookController)

	app.Post("/user-book", controller.UserBookListController)

	app.Get("/delete-user-book/{id}", controller.DeleteUserBookController)

	app.Post("/get-open-id", controller.GetOpenId)

	app.Run(iris.Addr(":8080"))
}
