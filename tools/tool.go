package tools

type Response struct {
	Data interface{} `json:"data"`
	Meta ErrMeta     `json:"meta"`
}

type ErrMeta struct {
	ErrCode int    `json:"err_code"`
	ErrMsg  string `json:"err_msg"`
}

func WrapResult(data interface{}, err error, errCode int) *Response {
	errMeta := new(ErrMeta)
	if err != nil {
		errMeta.ErrMsg = err.Error()
	} else {
		errMeta.ErrMsg = ""
	}
	errMeta.ErrCode = errCode

	response := new(Response)
	response.Meta = *errMeta

	response.Data = data
	return response
}
