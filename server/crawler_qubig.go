package main

import (
	"book-crawler/db"
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/gocolly/colly"
	"log"
	"strings"
)

func main() {
	//fromUrl := "https://www.xbiquge.cc/book/8449/"
	//getBook(fromUrl)
	findAllBook()
}

func getSection(fromUrl string, id int64) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.xbiquge.cc"),
	)
	// t := ""
	c.OnHTML(".box_con dd a[href],.box_con dt", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if strings.Trim(link, "") == "" {
			// t = e.Text
		} else {
			fmt.Printf("Link found: %q -> %s\n", coverGBKToUTF8(e.Text), link)

			// t1 := strings.Split(coverGBKToUTF8(e.Text), "-")[0]

			book := new(db.BookUrl)
			book.FromUrl = fromUrl
			// book.LinkText = coverGBKToUTF8(t) + t1
			book.LinkText = coverGBKToUTF8(e.Text)
			book.LinkUrl = coverGBKToUTF8(link)
			book.BookId = id

			result, err := db.FindBook(book.LinkUrl, book.FromUrl)

			fmt.Println(book)

			if err != nil {
				log.Printf("FindBook Inset failed, err: %v\n", err)
			}

			if result != nil {
				log.Printf("已存在: %v\n", book.FromUrl)
			} else {
				_, err = db.CreateBookUrl(book)
				if err != nil {
					log.Fatalf("create 失败")
				}
			}

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func getBook(fromUrl string) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.xbiquge.cc"),
	)

	c.OnHTML("body", func(e *colly.HTMLElement) {
		t := coverGBKToUTF8(e.ChildText(".box_con .con_top"))
		t = strings.Split(t, ">")[1]

		imageUrl := coverGBKToUTF8(e.ChildAttr("#maininfo #fmimg img", "src"))
		name := coverGBKToUTF8(e.ChildText("#maininfo #info h1"))
		author := coverGBKToUTF8(e.ChildText("#maininfo #info>p>a[target=_blank]"))
		description := coverGBKToUTF8(e.ChildText("#maininfo #intro"))

		book := new(db.BookCreateReq)
		book.BookType = "校园言情"
		book.Name = name
		book.ImageUrl = imageUrl
		book.Author = author
		book.Description = description

		id, err := db.CreateBook(book)
		if err != nil {
			log.Fatalf("create book error", err)
		}

		getSection(fromUrl, id)
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func findAllBook() {
	data, err := db.FindAllBook()
	if err != nil {
		log.Fatalf("find all book failed")
	}

	var preBookId int64 = 0

	for i := 0; i < len(data); i++ {
		id := data[i].Id
		url := data[i].FromUrl + data[i].LinkUrl
		bookUrlId := data[i].BookUrlId
		title := data[i].LinkText

		fmt.Println(preBookId, id, url, bookUrlId, title)
		getHtml(preBookId, id, url, bookUrlId, title)
		preBookId = id
	}
}

func getHtml(preBookId int64, bookId int64, fromUrl string, bookUrlId int64, title string) {

	if preBookId != bookId && preBookId != 0 {
		_, err := db.UpdateBookStatus(preBookId, "success")
		if err != nil {
			log.Fatal("UpdateBookStatus failed")
		}
		log.Printf("-----结束导入bookId为：%v的内容\n", preBookId)
	}

	c := colly.NewCollector(
		colly.AllowedDomains("www.xbiquge.cc"),
	)

	c.OnHTML("#content", func(e *colly.HTMLElement) {
		t, err := e.DOM.Html()
		if err != nil {
			log.Fatalf("获取html 失败 %v\n", err)
		} else {
			t = strings.ReplaceAll(coverGBKToUTF8(t), "聽聽聽聽", "")
			t = strings.ReplaceAll(t, "笔趣阁 www.xbiquge.cc，最快更新逢青最新章节！<br/><br/>", " ")
			t = strings.TrimSpace(t)
			log.Printf("-----开始导入%v的内容\n", title)

			article := new(db.CreateArticleReq)
			article.Title = title
			article.Text = t
			article.BookUrlId = bookUrlId
			article.BookId = bookId

			_, err = db.CreateArticle(article)
			if err != nil {
				log.Fatal("CreateArticle failed")
			}

			_, err := db.UpdateBookUrlStatus(bookUrlId, "success")
			if err != nil {
				log.Fatal("UpdateBookUrlStatus failed")
			}

			log.Printf("-----结束导入%v的内容\n", title)

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(response *colly.Response, err error) {
		log.Fatalf("coloy failed:%v\n", err)
	})

	c.Visit(fromUrl)
}

func coverGBKToUTF8(src string) string { return mahonia.NewDecoder("gbk").ConvertString(src) }
