package main

import (
	"book-crawler/db"
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/gocolly/colly"
	"log"
	"strings"
)

func main() {
	//fromUrl := "https://www.xshuyaya.com/book/15086/"
	//getBook1(fromUrl)

	// getSection1("https://www.xshuyaya.com/read/15086/", 17)
	findAllBook1()
}

func getSection1(fromUrl string, id int64) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.xshuyaya.com"),
	)
	// t := ""
	c.OnHTML(".dirwraps .dirconone ul li a", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if strings.Trim(link, "") == "" {
			// t = e.Text
		} else {
			fmt.Printf("Link found: %q -> %s\n", coverGBKToUTF81(e.Text), link)

			// t1 := strings.Split(coverGBKToUTF81(e.Text), "-")[0]

			book := new(db.BookUrl)
			book.FromUrl = fromUrl
			// book.LinkText = coverGBKToUTF81(t) + t1
			book.LinkText = e.Text
			book.LinkUrl = link
			book.BookId = id

			result, err := db.FindBook(book.LinkUrl, book.FromUrl)

			fmt.Println(book)

			if err != nil {
				log.Printf("FindBook Inset failed, err: %v\n", err)
			}

			if result != nil {
				log.Printf("已存在: %v\n", book.FromUrl)
			} else {
				_, err = db.CreateBookUrl(book)
				if err != nil {
					log.Fatalf("create 失败")
				}
			}
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func getBook1(fromUrl string) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.xshuyaya.com"),
	)

	c.OnHTML("body", func(e *colly.HTMLElement) {

		imageUrl := e.ChildAttr(".con_limg img", "src")
		name := e.ChildText(".r420 h2")
		author := e.ChildText(".r420 .author")
		description := e.ChildText(".r_cons")

		book := new(db.BookCreateReq)
		book.BookType = "言情小说"
		book.Name = name
		book.ImageUrl = imageUrl
		book.Author = author
		book.Description = description

		fmt.Println(book)

		id, err := db.CreateBook(book)
		if err != nil {
			log.Fatalf("create book error", err)
		}

		fmt.Println(id)

		// getSection1(fromUrl, id)
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func findAllBook1() {
	data, err := db.FindAllBook()
	if err != nil {
		log.Fatalf("find all book failed")
	}

	var preBookId int64 = 0

	for i := 0; i < len(data); i++ {
		id := data[i].Id
		url := data[i].LinkUrl
		bookUrlId := data[i].BookUrlId
		title := data[i].LinkText

		fmt.Println(preBookId, id, url, bookUrlId, title)
		getHtml1(preBookId, id, url, bookUrlId, title)
		preBookId = id
	}
}

func getHtml1(preBookId int64, bookId int64, fromUrl string, bookUrlId int64, title string) {

	if preBookId != bookId && preBookId != 0 {
		_, err := db.UpdateBookStatus(preBookId, "success")
		if err != nil {
			log.Fatal("UpdateBookStatus failed")
		}
		log.Printf("-----结束导入bookId为：%v的内容\n", preBookId)
	}

	c := colly.NewCollector(
		colly.AllowedDomains("www.xshuyaya.com"),
	)

	c.OnHTML("#content", func(e *colly.HTMLElement) {
		t, err := e.DOM.Html()
		if err != nil {
			log.Fatalf("获取html 失败 %v\n", err)
		} else {
			log.Printf("-----开始导入%v的内容\n", title)

			article := new(db.CreateArticleReq)
			article.Title = title
			article.Text = strings.TrimSpace(t)
			article.BookUrlId = bookUrlId
			article.BookId = bookId

			fmt.Println(article)

			_, err = db.CreateArticle(article)
			if err != nil {
				log.Fatal("CreateArticle failed")
			}

			_, err := db.UpdateBookUrlStatus(bookUrlId, "success")
			if err != nil {
				log.Fatal("UpdateBookUrlStatus failed")
			}

			log.Printf("-----结束导入%v的内容\n", title)

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(response *colly.Response, err error) {
		log.Fatalf("coloy failed:%v\n", err)
	})

	c.Visit(fromUrl)
}

func coverGBKToUTF81(src string) string { return mahonia.NewDecoder("gbk").ConvertString(src) }
