package main

import (
	"book-crawler/db"
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/gocolly/colly"
	"log"
	"strings"
)

func main() {
	//fromUrl := "http://www.jjwxc.net/onebook.php?novelid=61010"
	//getBook2(fromUrl)
	findAllBook2()
}

func getSection2(fromUrl string, id int64) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.jjwxc.net"),
	)

	c.OnHTML("table#oneboolt tr td span[itemprop=headline] a[itemprop=url]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if strings.Trim(link, "") == "" {
		} else {
			fmt.Printf("Link found: %q -> %s\n", e.Text, link)
			book := new(db.BookUrl)
			book.FromUrl = fromUrl
			book.LinkText = coverGBKToUTF82(e.Text)
			book.LinkUrl = link
			book.BookId = id

			fmt.Println(book)

			result, err := db.FindBook(book.LinkUrl, book.FromUrl)

			if err != nil {
				log.Printf("FindBook Inset failed, err: %v\n", err)
			}

			if result != nil {
				log.Printf("已存在: %v\n", book.FromUrl)
			} else {
				_, err = db.CreateBookUrl(book)
				if err != nil {
					log.Fatalf("create 失败")
				}
			}
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func getBook2(fromUrl string) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.jjwxc.net"),
	)

	c.OnHTML("body", func(e *colly.HTMLElement) {

		imageUrl := e.ChildAttr("img.noveldefaultimage", "src")
		name := e.ChildText("#oneboolt .bigtext h1 span[itemprop=articleSection]")
		author := e.ChildText("#oneboolt h2 span[itemprop=author]")
		description := e.ChildText("#novelintro")

		book := new(db.BookCreateReq)
		book.BookType = "奇幻小说"
		book.Name = coverGBKToUTF82(name)
		book.ImageUrl = imageUrl
		book.Author = coverGBKToUTF82(author)
		book.Description = coverGBKToUTF82(description)

		fmt.Println(book)

		data, err := db.FindBookByNameAndAuthor(book.Name, book.Author)
		if err != nil {
			log.Fatalf("FindBookByName error", err)
		}

		if data != nil {
			log.Println("book 已存在")
			getSection2(fromUrl, data.Id)
		} else {
			id, err := db.CreateBook(book)
			if err != nil {
				log.Fatalf("create book error", err)
			}
			getSection2(fromUrl, id)
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func findAllBook2() {
	data, err := db.FindAllBook()
	if err != nil {
		log.Fatalf("find all book failed")
	}

	var preBookId int64 = 0

	for i := 0; i < len(data); i++ {
		id := data[i].Id
		url := data[i].LinkUrl
		bookUrlId := data[i].BookUrlId
		title := data[i].LinkText

		fmt.Println(preBookId, id, url, bookUrlId, title)
		getHtml2(preBookId, id, url, bookUrlId, title)
		preBookId = id
	}
}

func getHtml2(preBookId int64, bookId int64, fromUrl string, bookUrlId int64, title string) {

	if preBookId != bookId && preBookId != 0 {
		_, err := db.UpdateBookStatus(preBookId, "success")
		if err != nil {
			log.Fatal("UpdateBookStatus failed")
		}
		log.Printf("-----结束导入bookId为：%v的内容\n", preBookId)
	}

	c := colly.NewCollector(
		colly.AllowedDomains("www.jjwxc.net"),
	)

	c.OnHTML(".noveltext", func(e *colly.HTMLElement) {

		e.DOM.Find("div").Remove()
		e.DOM.Find("hr").Remove()
		t, err := e.DOM.Html()

		if err != nil {
			log.Fatalf("获取html 失败 %v\n", err)
		} else {
			log.Printf("-----开始导入%v的内容\n", title)
			article := new(db.CreateArticleReq)
			article.Title = title
			article.Text = coverGBKToUTF82(strings.TrimSpace(t))
			article.BookUrlId = bookUrlId
			article.BookId = bookId

			_, err = db.CreateArticle(article)
			if err != nil {
				log.Fatal("CreateArticle failed")
			}

			_, err := db.UpdateBookUrlStatus(bookUrlId, "success")
			if err != nil {
				log.Fatal("UpdateBookUrlStatus failed")
			}

			log.Printf("-----结束导入%v的内容\n", title)

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(response *colly.Response, err error) {
		log.Fatalf("coloy failed:%v\n", err)
	})

	c.Visit(fromUrl)
}

func coverGBKToUTF82(src string) string { return mahonia.NewDecoder("gbk").ConvertString(src) }
