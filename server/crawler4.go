package main

import (
	"book-crawler/db"
	"fmt"
	"github.com/gocolly/colly"
	"log"
	"strings"
)

func main() {
	//fromUrl := "https://www.tianxiabachang.cn/0_472/"
	//getBook4(fromUrl)
	findAllBook4()
}

func getSection4(fromUrl string, id int64) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.tianxiabachang.cn"),
	)

	c.OnHTML(".box_con dd a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if strings.Trim(link, "") == "" {

		} else {
			fmt.Printf("Link found: %q -> %s\n", e.Text, link)
			book := new(db.BookUrl)
			book.FromUrl = fromUrl
			book.LinkText = e.Text
			book.LinkUrl = "https://www.tianxiabachang.cn" + link
			book.BookId = id

			fmt.Println(book)

			result, err := db.FindBook(book.LinkUrl, book.FromUrl)

			if err != nil {
				log.Printf("FindBook Inset failed, err: %v\n", err)
			}

			if result != nil {
				log.Printf("已存在: %v\n", book.FromUrl)
			} else {
				_, err = db.CreateBookUrl(book)
				if err != nil {
					log.Fatalf("create 失败")
				}
			}

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func getBook4(fromUrl string) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.tianxiabachang.cn"),
	)

	c.OnHTML("body", func(e *colly.HTMLElement) {
		//imageUrl := coverGBKToUTF8(e.ChildAttr("#maininfo #fmimg img", "src"))
		name := e.ChildText("#maininfo #info h1")
		// author := coverGBKToUTF8(e.ChildText("#maininfo #info>p>a[target=_blank]"))
		description := e.ChildText("#maininfo #intro p")

		book := new(db.BookCreateReq)
		book.BookType = "校园言情"
		book.Name = name
		book.ImageUrl = "http://i7-static.jjwxc.net/tmp/backend/authorspace/s1/20/19386/1938546/20200323114741.jpg"
		book.Author = "竹已"
		book.Description = description

		fmt.Println(book)

		data, err := db.FindBookByNameAndAuthor(book.Name, book.Author)
		if err != nil {
			log.Fatalf("FindBookByName error", err)
		}

		if data != nil {
			log.Println("book 已存在")
			getSection4(fromUrl, data.Id)
		} else {
			id, err := db.CreateBook(book)
			if err != nil {
				log.Fatalf("create book error", err)
			}
			getSection4(fromUrl, id)
		}
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func findAllBook4() {
	data, err := db.FindAllBook()
	if err != nil {
		log.Fatalf("find all book failed")
	}

	var preBookId int64 = 0

	for i := 0; i < len(data); i++ {
		id := data[i].Id
		url := data[i].LinkUrl
		bookUrlId := data[i].BookUrlId
		title := data[i].LinkText

		fmt.Println(preBookId, id, url, bookUrlId, title)
		getHtml4(preBookId, id, url, bookUrlId, title)
		preBookId = id
	}
}

func getHtml4(preBookId int64, bookId int64, fromUrl string, bookUrlId int64, title string) {

	if preBookId != bookId && preBookId != 0 {
		_, err := db.UpdateBookStatus(preBookId, "success")
		if err != nil {
			log.Fatal("UpdateBookStatus failed")
		}
		log.Printf("-----结束导入bookId为：%v的内容\n", preBookId)
	}

	c := colly.NewCollector(
		colly.AllowedDomains("www.tianxiabachang.cn"),
	)

	c.OnHTML("#content", func(e *colly.HTMLElement) {
		t, err := e.DOM.Html()
		if err != nil {
			log.Fatalf("获取html 失败 %v\n", err)
		} else {
			t = strings.TrimSpace(t)
			log.Printf("-----开始导入%v的内容\n", title)

			article := new(db.CreateArticleReq)
			article.Title = title
			article.Text = t
			article.BookUrlId = bookUrlId
			article.BookId = bookId

			// fmt.Println(article)

			_, err = db.CreateArticle(article)
			if err != nil {
				log.Fatal("CreateArticle failed")
			}

			_, err := db.UpdateBookUrlStatus(bookUrlId, "success")
			if err != nil {
				log.Fatal("UpdateBookUrlStatus failed")
			}

			log.Printf("-----结束导入%v的内容\n", title)

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(response *colly.Response, err error) {
		log.Fatalf("coloy failed:%v\n", err)
	})

	c.Visit(fromUrl)
}
