package main

import (
	"book-crawler/db"
	"fmt"
	"log"
	"strings"
)

func main() {
	result, err := db.FindAllArticle()

	if err != nil {
		log.Fatalf("find all article failed")
	}

	for i := 0; i < len(result); i++ {
		id := result[i].Id
		text := result[i].Text

		text = strings.TrimSpace(text)
		text = strings.ReplaceAll(text, "笔趣阁 www.xbiquge.cc，最快更新驱魔王妃最新章节！<br/><br/>", "")
		// text = strings.Split(text, `<div class="bzend"><span>本章结束</span></div>`)[0]
		text = strings.TrimSpace(text)
		fmt.Println(id, text)

		db.UpdateText(id, text)
		fmt.Println("----------")
	}

}
