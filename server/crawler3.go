package main

import (
	"book-crawler/db"
	"fmt"
	"github.com/axgle/mahonia"
	"github.com/gocolly/colly"
	"log"
	"strings"
)

func main() {
	//fromUrl := "http://www.my2852.com/xy/rj/index.htm"
	//getBook3(fromUrl)
	findAllBook3()
}

func getSection3(fromUrl string, id int64) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.my2852.com"),
	)

	c.OnHTML("table.tb5 td.td6 table tr td a", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if strings.Trim(link, "") == "" {
		} else {
			fmt.Printf("Link found: %q -> %s\n", e.Text, link)
			book := new(db.BookUrl)
			book.FromUrl = fromUrl
			book.LinkText = coverGBKToUTF83(e.Text)
			book.LinkUrl = link
			book.BookId = id

			fmt.Println(book)

			result, err := db.FindBook(book.LinkUrl, book.FromUrl)

			if err != nil {
				log.Printf("FindBook Inset failed, err: %v\n", err)
			}

			if result != nil {
				log.Printf("已存在: %v\n", book.FromUrl)
			} else {
				_, err = db.CreateBookUrl(book)
				if err != nil {
					log.Fatalf("create 失败")
				}
			}
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func getBook3(fromUrl string) {
	c := colly.NewCollector(
		colly.AllowedDomains("www.my2852.com"),
	)

	c.OnHTML("body", func(e *colly.HTMLElement) {

		//imageUrl := e.ChildAttr("img.noveldefaultimage", "src")
		//name := e.ChildText("#oneboolt .bigtext h1 span[itemprop=articleSection]")
		//author := e.ChildText("#oneboolt h2 span[itemprop=author]")
		description := e.ChildText("table.tb6 tbody tr:first-child td")

		book := new(db.BookCreateReq)
		book.BookType = "青春校园"
		book.Name = "若即若离"
		book.ImageUrl = "http://i9-static.jjwxc.net/novelimage.php?novelid=465432&coverid=12&ver=6df3c4c21d454b1070968ef65490067b"
		book.Author = "忽然之间"
		book.Description = coverGBKToUTF83(description)

		fmt.Println(book)

		data, err := db.FindBookByNameAndAuthor(book.Name, book.Author)
		if err != nil {
			log.Fatalf("FindBookByName error", err)
		}

		if data != nil {
			log.Println("book 已存在")
			getSection3(fromUrl, data.Id)
		} else {
			id, err := db.CreateBook(book)
			if err != nil {
				log.Fatalf("create book error", err)
			}
			getSection3(fromUrl, id)
		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit(fromUrl)
}

func findAllBook3() {
	data, err := db.FindAllBook()
	if err != nil {
		log.Fatalf("find all book failed")
	}

	var preBookId int64 = 0

	for i := 0; i < len(data); i++ {
		id := data[i].Id
		url := "http://www.my2852.com/xy/rj/" + data[i].LinkUrl
		bookUrlId := data[i].BookUrlId
		title := data[i].LinkText

		fmt.Println(preBookId, id, url, bookUrlId, title)
		getHtml3(preBookId, id, url, bookUrlId, title)
		preBookId = id
	}
}

func getHtml3(preBookId int64, bookId int64, fromUrl string, bookUrlId int64, title string) {

	if preBookId != bookId && preBookId != 0 {
		_, err := db.UpdateBookStatus(preBookId, "success")
		if err != nil {
			log.Fatal("UpdateBookStatus failed")
		}
		log.Printf("-----结束导入bookId为：%v的内容\n", preBookId)
	}

	c := colly.NewCollector(
		colly.AllowedDomains("www.my2852.com"),
	)

	c.OnHTML("table.tb>tbody>tr:nth-child(4)>td", func(e *colly.HTMLElement) {

		t, err := e.DOM.Html()

		if err != nil {
			log.Fatalf("获取html 失败 %v\n", err)
		} else {
			log.Printf("-----开始导入%v的内容\n", title)
			article := new(db.CreateArticleReq)
			article.Title = title
			article.Text = coverGBKToUTF83(strings.TrimSpace(t))
			article.BookUrlId = bookUrlId
			article.BookId = bookId

			// fmt.Println(article)

			_, err = db.CreateArticle(article)
			if err != nil {
				log.Fatal("CreateArticle failed")
			}

			_, err := db.UpdateBookUrlStatus(bookUrlId, "success")
			if err != nil {
				log.Fatal("UpdateBookUrlStatus failed")
			}

			log.Printf("-----结束导入%v的内容\n", title)

		}

	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnError(func(response *colly.Response, err error) {
		log.Fatalf("coloy failed:%v\n", err)
	})

	c.Visit(fromUrl)
}

func coverGBKToUTF83(src string) string { return mahonia.NewDecoder("gbk").ConvertString(src) }
